struct render_uniforms
{
    float4x4 WVPTransform;
    float4x4 WTransform;
    float4x4 NormalWTransform;
    float Shininess;
    float SpecularStrength;
    bool IsPointLight;
};

cbuffer RenderUniforms : register(b0)
{
    render_uniforms RenderUniforms;
};

struct vs_input
{
    float3 Position : POSITION;
    float2 Uv : TEXCOORD0;
    float3 Normal : NORMAL;
};

struct ps_input
{
    float4 Position : SV_POSITION;
    float3 WorldPos : POSITION;
    float2 Uv : TEXCOORD0;
    float3 Normal : NORMAL;
};

ps_input ModelVsMain(vs_input Input)
{
    ps_input Result;

    Result.Position = mul(RenderUniforms.WVPTransform, float4(Input.Position, 1.0f));
    Result.WorldPos = mul(RenderUniforms.WTransform, float4(Input.Position, 1.0f)).xyz;
    Result.Uv = Input.Uv;
    Result.Normal = normalize(mul(RenderUniforms.NormalWTransform, float4(Input.Normal, 0.0f)).xyz);

    return Result;
}