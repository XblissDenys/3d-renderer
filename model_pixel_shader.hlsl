Texture2D Texture : register(t0);
SamplerState BilinearSampler : register(s0);

struct render_uniforms
{
    float4x4 WVPTransform;
    float4x4 WTransform;
    float4x4 NormalWTransform;
    float Shininess;
    float SpecularStrength;
    bool IsPointLight;
    uint PointLightId;
};

cbuffer RenderUniforms : register(b0)
{
    render_uniforms RenderUniforms;
};

struct dir_light_uniforms
{
    float3 Color;
    float AmbientIntensity;
    float3 Direction;
    uint NumPointLights;
    float3 CameraPos;
};

cbuffer LightUniforms : register(b1)
{
    dir_light_uniforms DirLightUniforms;
};

//struct point_light
//{
//    float3 Pos;
//    float DivisorConstant;
//    float3 Color;
//    float pad0;
//};

//StructuredBuffer<point_light> PointLightBuffer : register(t1);

#define MAX_LIGHTS 2

//cbuffer PointLightUniforms : register(b2)
//{
//    point_light PointLightBuf[MAX_LIGHTS];
//};

struct cpoint_light
{
    float3 Pos;
    float pad0;
    float3 Color;
    float DivisorConstant;
};

cbuffer CPointLightsUniform : register(b2)
{
    cpoint_light PointLightsBuffer[MAX_LIGHTS];
}

struct ps_input
{
    float4 Position : SV_POSITION;
    float3 WorldPos : POSITION;
    float2 Uv : TEXCOORD0;
    float3 Normal : NORMAL;
};

struct ps_output
{
    float4 Color : SV_TARGET0;
};

float3 EvaluatePhong(float3 SurfaceColor, float3 SurfaceNormal, float3 SurfaceWorldPos,
                     float SurfaceShininess, float SurfaceSpecularStrength,
                     float3 LightDirection, float3 LightColor, float LightAmbientIntensity)
{
    float3 NegativeLightDir = -LightDirection;
    float AccumIntensity = LightAmbientIntensity;
    
    // NOTE: Diffuse reflection
    {
        float DiffuseIntensity = max(0, dot(SurfaceNormal, NegativeLightDir));
        AccumIntensity += DiffuseIntensity;
    }

    // NOTE: Specular reflection
    float SpecularIntensity = 0;
    {
        float3 CameraDirection = normalize(DirLightUniforms.CameraPos - SurfaceWorldPos);
        float3 HalfVector = normalize(NegativeLightDir + CameraDirection);
        SpecularIntensity = SurfaceSpecularStrength * pow(max(0, dot(HalfVector, SurfaceNormal)), SurfaceShininess * 2);
    }
    
    float3 MixedColor = LightColor * SurfaceColor;
    float3 Result = AccumIntensity * MixedColor + SpecularIntensity * LightColor;

    return Result;
}

ps_output ModelPsMain(ps_input Input)
{
    ps_output Result;
    float4 SurfaceColor = Texture.Sample(BilinearSampler, Input.Uv);
    float3 SurfaceNormal = normalize(Input.Normal);
    if (SurfaceColor.a == 0.0f)
    {
        discard;
    }
    
    if (RenderUniforms.IsPointLight)
    {
        //Result.Color.rgb = float3(0, 0, 0);
        Result.Color.rgb = PointLightsBuffer[RenderUniforms.PointLightId].Color;
        Result.Color.a = SurfaceColor.a;
    }
    if (!RenderUniforms.IsPointLight)
    {
        Result.Color.rgb = float3(0, 0, 0);
        // NOTE: light pixel with direct light
        //Result.Color.rgb = EvaluatePhong(SurfaceColor.rgb, SurfaceNormal, Input.WorldPos,
        //                            RenderUniforms.Shininess, RenderUniforms.SpecularStrength,
        //                            DirLightUniforms.Direction, DirLightUniforms.Color, DirLightUniforms.AmbientIntensity);
    
    // NOTE: light pixel with point light
        for (int PointLightId = 0; PointLightId < DirLightUniforms.NumPointLights; ++PointLightId)
        {
            cpoint_light PointLight = PointLightsBuffer[PointLightId];

        // NOTE: Calculating light attenuation
            float3 VectorToLight = Input.WorldPos - PointLight.Pos;
            float Radius = length(VectorToLight);
            float3 AttenuatedLight = PointLight.Color / (PointLight.DivisorConstant + Radius * Radius);

            VectorToLight /= Radius;
            Result.Color.rgb += EvaluatePhong(SurfaceColor.rgb, SurfaceNormal, Input.WorldPos,
                                          RenderUniforms.Shininess, RenderUniforms.SpecularStrength,
                                          VectorToLight, AttenuatedLight, 0);
        }
    
        Result.Color.a = SurfaceColor.a;
    }
    

    return Result;
}